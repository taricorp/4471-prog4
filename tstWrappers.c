#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char** argv){
    if (argc < 2) {
        printf("Usage: %s file\n", argv[0]);
        return 1;
    }

    struct stat fileStat;
    
    int file = open(argv[1],O_RDONLY);
    if (file == -1) {
        perror(argv[1]);
        return 1;
    }

    if (fstat(file, &fileStat) == -1) {
        perror(argv[1]);
        return 1;
    }

    printf("File Size: %d\n", fileStat.st_size);
    char buffer[8];
    ssize_t cnt = read(file, buffer, 8);
    if (cnt == -1) {
        perror(argv[1]);
        return 1;
    }

    printf("First %u bytes: ", cnt);
    for (int i = 0; i < (cnt < 8 ? cnt : 8); i++) {
        printf("%#02hhx ", buffer[i]);
    }
    putchar('\n');

    close(file);

    return 0;
}
